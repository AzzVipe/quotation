import * as Realm from "realm-web";
import axios from "axios";

export const useMyRealmApp = () => {
	const appId = useRuntimeConfig().public.APP_ID;

	const app = new Realm.App({
		id: appId,
	});

	const currentUser = app.currentUser;

	const darkMode = ref(false);

	const setDarkMode = () => {
		darkMode.value = true;
	};

	const unsetDarkMode = () => {
		darkMode.value = false;
	};

	const issetDarkMode = () => {
		return darkMode.value;
	};

	const getAddressSuggestions = async (postalCode) => {
		let data;
		const apiKey = useRuntimeConfig().public.ADDRESS_API_KEY;
		const config = {
			method: "get",
			maxBodyLength: Infinity,
			url: `https://api.getAddress.io/autocomplete/${postalCode}?api-key=${apiKey}&top=20`,
		};

		await axios(config)
			.then(function (response) {
				data = response.data;
				console.log(response);
			})
			.catch(function (error) {
				console.log(error);
			});

		return data;
	};

	const getAddressByID = async (addressID) => {
		let data;
		const apiKey = useRuntimeConfig().public.ADDRESS_API_KEY;
		const config = {
			method: "get",
			maxBodyLength: Infinity,
			url: `https://api.getAddress.io/get/${addressID}?api-key=${apiKey}`,
		};

		await axios(config)
			.then(function (response) {
				data = response.data;
			})
			.catch(function (error) {
				console.log(error);
			});

		return data;
	};

	return {
		app,
		Realm,
		currentUser,
		getAddressSuggestions,
		getAddressByID,
		setDarkMode,
		unsetDarkMode,
		darkMode,
	};
};
